package cn.barudisshu.cglib;

import java.lang.reflect.Method;

/**
 * @author Barudisshu
 */
public interface AOPInterceptor {

    void before(Method method, Object[] args);
    void after(Method method, Object[] args);
    void afterThrowing(Method method, Object[] args, Throwable throwable);
    void afterFinally(Method method, Object[] args);
}