package cn.barudisshu.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

public class CGLIBProxyFactory {

    private CGLIBProxyFactory() {
    }

    @SuppressWarnings("unchecked")
    public static <T> T createProxy(T target, AOPInterceptor interceptor) {

        MethodInterceptor methodInterceptor = new CGLIBMethodInterceptor(interceptor);

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setInterfaces(target.getClass().getInterfaces());
        enhancer.setCallback(methodInterceptor);
        return (T) enhancer.create();
    }
}