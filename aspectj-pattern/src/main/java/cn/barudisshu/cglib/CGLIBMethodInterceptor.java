package cn.barudisshu.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author Barudisshu
 */
public class CGLIBMethodInterceptor implements MethodInterceptor {

    private AOPInterceptor interceptor;

    public CGLIBMethodInterceptor(AOPInterceptor interceptor) {
        this.interceptor = interceptor;
    }

    public Object intercept(Object object, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        try {
            interceptor.before(method, args);
            Object returnValue = methodProxy.invokeSuper(object, args);
            interceptor.after(method, args);
            return returnValue;
        } catch (Throwable t) {
            interceptor.afterThrowing(method, args, t);
            throw t;
        } finally {
            interceptor.afterFinally(method, args);
        }
    }
}