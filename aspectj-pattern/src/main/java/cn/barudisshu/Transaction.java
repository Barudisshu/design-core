package cn.barudisshu;

/**
 * @author Barudisshu
 */
public interface Transaction {
    void open();
    void rollBack();
    void commit();
    void closeIfStillOpen();
}
