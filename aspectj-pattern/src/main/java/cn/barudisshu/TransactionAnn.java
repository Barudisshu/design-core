package cn.barudisshu;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.sql.Connection;

/**
 * @author Barudisshu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface TransactionAnn {
    String[] name() default {};

    boolean[] readonly() default false;

    int[] level() default Connection.TRANSACTION_READ_COMMITTED;

    String value() default "";
}
