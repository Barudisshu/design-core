package cn.barudisshu.aspect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author Barudisshu
 */
public interface Aspect {

  public Object aspect(InvocationHandler ih, Object proxy, Method method, Object[] args) throws Throwable;

}
