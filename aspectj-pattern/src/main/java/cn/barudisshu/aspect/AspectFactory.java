package cn.barudisshu.aspect;

import java.lang.reflect.Proxy;

/**
 * 代理工厂类
 * @author Barudisshu
 */
public class AspectFactory {


    /**单例实现*/
    private AspectFactory(){

    }

    @SuppressWarnings("unchecked")
    public static <T> T getInstance(T target,Aspect... aspects){
        AspectHandler handler = new AspectHandler(target, aspects);
        Class clazz = target.getClass();
        return (T) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), clazz.getInterfaces(), handler);

    }
}
