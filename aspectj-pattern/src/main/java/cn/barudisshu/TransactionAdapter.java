package cn.barudisshu;

/**
 * @author Barudisshu
 */
public class TransactionAdapter implements Transaction {

    @Override
    public void open() {
        System.out.println("open .... ");
    }

    @Override
    public void rollBack() {
        System.out.println("rollBack ... ");
    }

    @Override
    public void commit() {
        System.out.println("commit ... ");
    }

    @Override
    public void closeIfStillOpen() {
        System.out.println("close if still open ...");
    }
}
