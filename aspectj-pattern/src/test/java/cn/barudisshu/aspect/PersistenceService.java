package cn.barudisshu.aspect;

import cn.barudisshu.TransactionAnn;

import java.sql.Connection;

/**
 * @author Barudisshu
 */
public interface PersistenceService {

    @TransactionAnn(name = "save")
    void save(long id,String data);

    @TransactionAnn(level = Connection.TRANSACTION_READ_UNCOMMITTED)
    String load(long id);
}
