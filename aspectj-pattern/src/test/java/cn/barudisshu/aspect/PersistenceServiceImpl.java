package cn.barudisshu.aspect;

import cn.barudisshu.exception.TransactionException;

/**
 * @author Barudisshu
 */
public class PersistenceServiceImpl implements PersistenceService {

    @Override
    public void save(long id, String data) {
        throw new TransactionException("异常则回滚！！！");
    }

    @Override
    public String load(long id) {
        return null;
    }
}
