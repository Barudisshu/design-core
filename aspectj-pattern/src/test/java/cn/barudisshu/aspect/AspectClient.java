package cn.barudisshu.aspect;

import cn.barudisshu.aspect.transaction.TransactionAspect;

/**
 * @author Barudisshu
 */
public class AspectClient {

    public static void main(String[] args) {

        PersistenceService persistenceService = AspectFactory.getInstance(new PersistenceServiceImpl(), new TransactionAspect());

        persistenceService.save(3,"321");
    }
}
