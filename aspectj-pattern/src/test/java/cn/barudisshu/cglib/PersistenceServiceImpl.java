package cn.barudisshu.cglib;

import cn.barudisshu.TransactionAnn;

/**
 * @author Barudisshu
 */
public class PersistenceServiceImpl implements PersistenceService {

    @TransactionAnn("REQUIRES_NEW")
    public void save(long id, String data) {
        System.out.println("Generally save ... ");
    }

    @TransactionAnn("NOT_SUPPORTED")
    public String load(long id) {
        return null;
    }
}
