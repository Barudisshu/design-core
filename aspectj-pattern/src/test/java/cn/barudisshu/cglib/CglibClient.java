package cn.barudisshu.cglib;

import cn.barudisshu.cglib.transaction.TransactionCglib;

/**
 * @author Barudisshu
 */
public class CglibClient {

    public static void main(String[] args) {
        PersistenceService persistenceService = CGLIBProxyFactory.createProxy(new PersistenceServiceImpl(), new TransactionCglib());

        persistenceService.save(3L, "321");
    }
}
