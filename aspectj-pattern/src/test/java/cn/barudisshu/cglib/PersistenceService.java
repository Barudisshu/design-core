package cn.barudisshu.cglib;

/**
 * @author Barudisshu
 */
public interface PersistenceService {

    void save(long id, String data);

    String load(long id);
}
