package cn.barudisshu.state;

import org.junit.Before;
import org.junit.Test;

/**
 * @author galudisu
 */
public class DoorTest {

    private Door door;

    @Before
    public void init() throws Exception {
        door = new Door();
    }

    @Test
    public void touch() throws Exception {
        door.touch();
        System.out.println(door.status());
    }

    @Test
    public void complete() throws Exception {
        door.complete();
        System.out.println(door.status());
    }

    @Test
    public void timeout() throws Exception {
        door.timeout();
        System.out.println(door.status());
    }


}