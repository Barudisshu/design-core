package cn.barudisshu.state;

/**
 * @author galudisu
 */
public class DoorOpen extends DoorState {
    public DoorOpen(Door door) {
        super(door);
    }

    @Override
    public void touch() {
        door.setState(door.OPEN);
    }
}
