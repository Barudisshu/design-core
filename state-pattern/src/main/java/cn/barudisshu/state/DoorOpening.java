package cn.barudisshu.state;

/**
 * @author galudisu
 */
public class DoorOpening extends DoorState {
    public DoorOpening(Door door) {
        super(door);
    }

    @Override
    public void touch() {
        door.setState(door.OPENING);
    }
}
