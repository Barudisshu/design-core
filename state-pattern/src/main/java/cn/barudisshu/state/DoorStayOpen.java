package cn.barudisshu.state;

/**
 * @author galudisu
 */
public class DoorStayOpen extends DoorState {

    public DoorStayOpen(Door door) {
        super(door);
    }

    @Override
    public void touch() {
        door.setState(door.STAYOPEN);
    }

    @Override
    public void complete() {
        door.setState(door.CLOSED);
    }

    @Override
    public void timeout() {
        door.setState(door.CLOSED);
    }
}
