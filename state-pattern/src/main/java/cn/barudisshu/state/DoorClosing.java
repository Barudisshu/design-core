package cn.barudisshu.state;

/**
 * @author galudisu
 */
public class DoorClosing extends DoorState {
    public DoorClosing(Door door) {
        super(door);
    }

    @Override
    public void touch() {
        door.setState(door.CLOSING);
    }
}
