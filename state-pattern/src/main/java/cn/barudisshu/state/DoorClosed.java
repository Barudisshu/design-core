package cn.barudisshu.state;

/**
 * @author galudisu
 */
public class DoorClosed extends DoorState {
    public DoorClosed(Door door) {
        super(door);
    }

    @Override
    public void touch() {
        door.setState(door.CLOSED);
    }
}
