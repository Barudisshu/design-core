package cn.barudisshu.scheduler;

import java.util.HashMap;

/**
 * @author barudisshu
 */
public class JobDetail {

    private Class<? extends Job> clazz;
    private String jobName;
    private HashMap<String,Object> jobData;

    public JobDetail() {
        jobData = new HashMap<>();
    }

    public JobDetail(String jobName,Class<? extends Job> clazz) {
        this();
        this.clazz = clazz;
        this.jobName = jobName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || o.getClass().isInstance(getClass()))
            return false;

        JobDetail jobDetail = (JobDetail) o;

        return jobName != null ? !jobName.equals(jobDetail.jobName) : jobDetail.jobName != null;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((jobName == null) ? 0 : jobName.hashCode());
        return result;
    }

    public Class<? extends Job> getClazz() {
        return clazz;
    }

    public void setClazz(Class<? extends Job> clazz) {
        this.clazz = clazz;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public HashMap<String, Object> getJobData() {
        return jobData;
    }

    public void setJobData(HashMap<String, Object> jobData) {
        this.jobData = jobData;
    }
}
