package cn.barudisshu.scheduler;

import org.joda.time.LocalDateTime;

import java.util.Map;

/**
 * @author barudisshu
 */
public class Job {

    public void execute(Map<String,Object> jobData){
        System.out.println("#########################");
        System.out.println(jobData.get("type") + " :Test Job Run at: " + LocalDateTime.now());
        System.out.println("#########################");
    }
}
