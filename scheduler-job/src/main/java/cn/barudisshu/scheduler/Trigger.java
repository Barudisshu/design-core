package cn.barudisshu.scheduler;

/**
 * @author barudisshu
 */
public class Trigger implements Comparable<Trigger> {

    private String jobKey;
    private long nextFireTime;

    @Override
    public int compareTo(Trigger o) {
        return (int)(this.nextFireTime - o.nextFireTime);
    }

    public void resert(){
        setNextFireTime(-1);
    }

    public String getJobKey() {
        return jobKey;
    }

    public void setJobKey(String jobKey) {
        this.jobKey = jobKey;
    }

    public long getNextFireTime() {
        return nextFireTime;
    }

    public void setNextFireTime(long nextFireTime) {
        this.nextFireTime = nextFireTime;
    }
}
