package cn.barudisshu.aop;

/**
 * @author Barudisshu
 */
public aspect HelloWorld {

    pointcut callPointcut():
        call(void cn.barudisshu.aop.MyClass.foo(int,java.lang.String));

    before():callPointcut(){
        System.out.println("Hello World");
        System.out.println("In the advice attached to the call pointcut");
    }
    after():callPointcut(){
        System.out.println("All things has done");
    }
}
