package cn.barudisshu.handler;

import cn.barudisshu.handler.route.RouteHandler;
import cn.barudisshu.loaders.ResourceLoader;
import cn.barudisshu.route.RouteBuilder;

import javax.servlet.ServletContext;

/**
 * @author galudisu
 */
public final class RestIniter {

    private static final ResourceLoader RESOURCE_LOADER = new ResourceLoader();
    private Handler handler;
    private ServletContext servletContext;
    private Config config;

    public RestIniter(ServletContext servletContext, Config config) {
        this.servletContext = servletContext;
        this.config = config;

        // 配置资源
        config.configResource(RESOURCE_LOADER);
        RESOURCE_LOADER.build();

        RouteBuilder routeBuilder = new RouteBuilder(RESOURCE_LOADER);
        routeBuilder.build();
        handler = new RouteHandler(routeBuilder);

        //
        config.afterStart();
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public void stop() {
        config.beforeStop();
    }
}
