package cn.barudisshu.handler;

import cn.barudisshu.http.servlet.HttpRequest;
import cn.barudisshu.http.servlet.HttpResponse;

/**
 * @author galudisu
 */
public abstract class Handler {

    protected Handler nextHandler;

    public abstract void handle(HttpRequest req, HttpResponse res, boolean isHandled);
}
