package cn.barudisshu.handler;


import cn.barudisshu.loaders.ResourceLoader;

/**
 * Config.
 * <p/>
 * Config order: configConstant(), configResource(), configPlugin(), configInterceptor(), configHandler()
 */
public class Config {

  /**
   * Config resource
   */
  public void configResource(ResourceLoader resourceLoader) {
  }

  /**
   * Call back after Resty start
   */
  public void afterStart() {
  }

  /**
   * Call back before Resty stop
   */
  public void beforeStop() {
  }

}