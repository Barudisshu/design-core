package cn.barudisshu.sample.resource;

import cn.barudisshu.annotations.API;
import cn.barudisshu.annotations.GET;
import cn.barudisshu.http.Resource;
import cn.barudisshu.sample.model.User;

/**
 * @author galudisu
 */
@API("/tests")
public class Restful extends Resource{

    @GET("api/{id}")
    public User get(String id){
        System.out.println("参数 `id` 将被输出：" + id);
        System.out.println("RESTful API设计成功！ 请按照需要进行功能扩展");

        User user = new User(id,"Chen","Yanyi","10000",26);
        return user;
    }
}
