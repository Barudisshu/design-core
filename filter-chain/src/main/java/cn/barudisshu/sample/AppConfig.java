package cn.barudisshu.sample;

import cn.barudisshu.handler.Config;
import cn.barudisshu.loaders.ResourceLoader;

/**
 * @author galudisu
 */
public class AppConfig extends Config {

    @Override
    public void configResource(ResourceLoader resourceLoader) {
        resourceLoader.addIncludePackages("cn.barudisshu.sample.resource");
    }

    @Override
    public void afterStart() {
        super.afterStart();
    }

    @Override
    public void beforeStop() {
        super.beforeStop();
    }
}
