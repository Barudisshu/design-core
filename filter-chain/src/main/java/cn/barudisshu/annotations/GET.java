package cn.barudisshu.annotations;

import java.lang.annotation.*;

/**
 * @author galudisu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface GET {

    String value() default "";

}
