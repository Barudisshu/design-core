package cn.barudisshu.render;


import cn.barudisshu.http.ContentType;
import cn.barudisshu.http.servlet.HttpRequest;
import cn.barudisshu.http.servlet.HttpResponse;
import com.alibaba.fastjson.JSON;

/**
 * Created by ice on 14-12-29.
 *
 * @JsonerFiled(serialize=false)
 */
public class JsonRender extends Render {

  public void render(HttpRequest request, HttpResponse response, Object out) {
    if (out != null) {
      response.setContentType(ContentType.JSON);
      if (out instanceof String) {
        write(request, response, "\"" + out + "\"");
      } else {
        String json = JSON.toJSONString(out);
        write(request, response, json);
      }
    }
  }
}
