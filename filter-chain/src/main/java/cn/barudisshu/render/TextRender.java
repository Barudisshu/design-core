package cn.barudisshu.render;


import cn.barudisshu.http.ContentType;
import cn.barudisshu.http.servlet.HttpRequest;
import cn.barudisshu.http.servlet.HttpResponse;

/**
 * Created by ice on 14-12-29.
 */
public class TextRender extends Render {

  public void render(HttpRequest request, HttpResponse response, Object out) {
    if (out != null) {
      response.setContentType(ContentType.TEXT);
      write(request, response, "\"" + out + "\"");
    }
  }

}
