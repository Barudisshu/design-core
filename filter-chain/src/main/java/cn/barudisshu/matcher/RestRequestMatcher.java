package cn.barudisshu.matcher;

import com.google.common.base.Optional;

/**
 * User: xavierhanin
 * Date: 1/19/13
 * Time: 7:53 AM
 */
public interface RestRequestMatcher {
    Optional<? extends RestRequestMatch> match(String method, String path);
}
