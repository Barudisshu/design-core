package cn.barudisshu.http.servlet;

import cn.barudisshu.http.AbstractResponse;
import cn.barudisshu.http.HttpStatus;
import cn.barudisshu.http.Response;
import org.joda.time.Duration;

import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * User: xavierhanin
 * Date: 2/6/13
 * Time: 9:40 PM
 */
public class HttpResponse extends AbstractResponse<HttpServletResponse> {
    private final HttpServletResponse resp;
    private final javax.servlet.http.HttpServletRequest request;

    public HttpResponse(HttpServletResponse resp, javax.servlet.http.HttpServletRequest request) {
        super(HttpServletResponse.class, resp);
        this.resp = resp;
        this.request = request;
    }

    @Override
    protected void doSetStatus(HttpStatus httpStatus) {
        resp.setStatus(httpStatus.getCode());
    }

    @Override
    protected OutputStream doGetOutputStream() throws IOException {
        return resp.getOutputStream();
    }

    @Override
    protected void closeResponse() throws IOException {
    }

    @Override
    public Response addCookie(String cookie, String value, Duration expiration) {
        Cookie existingCookie = HttpRequest.getCookie(request.getCookies(), cookie);
        if (existingCookie != null) {
            if ("/".equals(existingCookie.getPath())
                    || existingCookie.getPath() == null // in some cases cookies set on path '/' are returned with a null path
                    ) {
                // update existing cookie
                existingCookie.setPath("/");
                existingCookie.setValue(value);
                existingCookie.setMaxAge(expiration.getStandardSeconds() > 0 ? (int) expiration.getStandardSeconds() : -1);
                resp.addCookie(existingCookie);
            } else {
                // we have an existing cookie on another path: clear it, and add a new cookie on root path
                existingCookie.setValue("");
                existingCookie.setMaxAge(0);
                resp.addCookie(existingCookie);

                Cookie c = new Cookie(cookie, value);
                c.setPath("/");
                c.setMaxAge(expiration.getStandardSeconds() > 0 ? (int) expiration.getStandardSeconds() : -1);
                resp.addCookie(c);
            }
        } else {
            Cookie c = new Cookie(cookie, value);
            c.setPath("/");
            c.setMaxAge(expiration.getStandardSeconds() > 0 ? (int) expiration.getStandardSeconds() : -1);
            resp.addCookie(c);
        }
        return this;
    }

    @Override
    public Response clearCookie(String cookie) {
        Cookie existingCookie = HttpRequest.getCookie(request.getCookies(), cookie);
        if (existingCookie != null) {
            existingCookie.setPath("/");
            existingCookie.setValue("");
            existingCookie.setMaxAge(0);
            resp.addCookie(existingCookie);
        }
        return this;
    }

    @Override
    public void doSetHeader(String headerName, String header) {
        resp.setHeader(headerName, header);
    }

    public void doAddHeader(String headerName, String header) {
        resp.addHeader(headerName, header);
    }


    @Override
    @SuppressWarnings("unchecked")
    public <T> T unwrap(Class<T> clazz) {
        if (clazz == HttpServletResponse.class || clazz == ServletResponse.class) {
            return (T) resp;
        }
        throw new IllegalArgumentException("underlying implementation is HttpServletResponse, not " + clazz.getName());
    }
    public void reset() {
        resp.reset();
    }
}
