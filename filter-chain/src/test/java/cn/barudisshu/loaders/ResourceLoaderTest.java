package cn.barudisshu.loaders;

import cn.barudisshu.annotations.API;
import cn.barudisshu.annotations.GET;
import cn.barudisshu.http.Resource;
import cn.barudisshu.matcher.RestRequestMatch;
import cn.barudisshu.matcher.StdRestRequestMatcher;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author galudisu
 */
public class ResourceLoaderTest {

    @org.junit.Test
    public void test() {
        ResourceLoader loader = new ResourceLoader();
        loader.addIncludePackages("cn.barudisshu.api");
        loader.build();

        Set<Class<? extends Resource>> resources = loader.getResources();

        for (Class<? extends Resource> resource : resources) {
            API api = resource.getAnnotation(API.class);
            System.out.println(resource);
            System.out.println(api);
            Method[] methods;

            String apiPath = getApi(resource);

            //自己的方法
            if (Modifier.isAbstract(resource.getSuperclass().getModifiers())) {
                methods = resource.getMethods();
            } else {
                methods = resource.getDeclaredMethods();
            }



            //遍历方法看是不是 restful api
            for (Method method : methods) {
                GET get = method.getAnnotation(GET.class);
                if (get != null) {
                    System.out.println(get);
                    System.out.println(get.value());

                    String regexPath = getApi(apiPath,get.value());
                    System.out.println(regexPath);

                    StdRestRequestMatcher matcher = new StdRestRequestMatcher("GET", regexPath);

                    Optional<? extends RestRequestMatch> match = matcher.match("GET", "/tests/api/1");
                    assertThat(match.isPresent()).isTrue();
                    assertThat(match.get().getPathParams()).isEqualTo(ImmutableMap.of("id", "1"));
                    System.out.println("==========URL 参数有 ===========");
                    System.out.println(match.get().getPathParams());

                }
            }

        }

    }

    /**
     * 获取api部分
     *
     * @param resourceClazz resource class
     * @return url apiPath
     */
    private String getApi(Class<? extends Resource> resourceClazz) {
        API api;
        String apiPath = "";
        api = resourceClazz.getAnnotation(API.class);
        if (api != null) {
            apiPath = api.value();
            if (!apiPath.equals("")) {
                if (!apiPath.startsWith("/")) {
                    apiPath = "/" + apiPath;
                }
            }
        }
        Class<?> superClazz = resourceClazz.getSuperclass();
        if (Resource.class.isAssignableFrom(superClazz)) {
            apiPath = getApi((Class<? extends Resource>) superClazz) + apiPath;
        }
        return apiPath;
    }

    /**
     * 最终生成的apiPath
     *
     * @param apiPath
     * @param methodPath
     * @return
     */
    private String getApi(String apiPath, String methodPath) {
        if (!methodPath.equals("")) {
            if (!methodPath.startsWith("/")) {
                apiPath = apiPath + "/" + methodPath;
            } else {
                apiPath = apiPath + methodPath;
            }
        }
        return apiPath;
    }
}