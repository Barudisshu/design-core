### 设计模式

1. `aop-inject` aspectj切面编程
2. `aspectj-pattern` 代理模式
3. `fork-join` fork and join 
4. `scheduler-job` Quartz-like design
5. `filter-chain` RESTful 设计 【责任链模式】【装饰器模式】【工厂方法】，主要参考有：
    - [如何设计REST API](http://blog.octo.com/en/design-a-rest-api/)
    - [Resty](https://github.com/Dreampie/Resty)
    - [Restx](https://github.com/restx/restx)
    - [HTTP API设计指南](https://github.com/Dreampie/http-api-design-ZH_CN)
6. `state-pattern` 状态模式，更好地实现状态机

